# Pyplates

[![License](https://img.shields.io/badge/Licence-MIT-green.svg)](LICENSE.md)

## Description

This software creates a document based in jinja templates.

Data are placed in bases in `toml` format.

The files generated are saved in `~/Downloads` directory for default.
See documentation `python main.py -h` to  change default values.

## Requirements

- python 3.10.0
- toml
- jinja2

## Usage

```{bash}
python main.py
```

```
pip install toml
python main.py -t base.micro -o ~/.config/micro/colorschemes
```
