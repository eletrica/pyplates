(deftheme {{ key_name }})

(custom-theme-set-faces
'{{ key_name }}

    '(default ((t (
        :inherit nil
        :extend nil
        :stipple nil
        :foreground "{{ foreground }}"
        :background "{{ background }}"
        :inverse-video nil
        :box nil
        :strike-through nil
        :overline nil
        :underline nil
        :slant normal
        :weight regular
        :height 158
        :width normal
        :foundry "PfEd"
        :family "MesloLGS Nerd Font"))))

    '(cursor ((t (:foreground "{{ color0 }}" :background "{{ foreground }}"))))
    '(fringe ((t (:background "{{ background }}"))))
    '(menu ((t (:background "{{ background }}"))))
    '(scroll-bar ((t (:background "{{ background }}"))))
    '(tool-bar ((t (:background "{{ background }}"))))
    '(vertical-border ((t (:background "{{ background }}"))))

    '(appt-notification ((t (:foreground "{{ color2 }}" :background "{{ background }}"))))
    '(blink-matching-paren-offscreen ((t (:foreground "{{ color11 }}" :background "{{ background }}"))))
    '(buffer-menu-buffer ((t (:foreground "{{ color4 }}" :background "{{ background }}"))))
    '(child-frame-border ((t (:foreground "{{ color3 }}" :background "{{ background }}"))))
    '(comint-highlight-input ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(comint-highlight-prompt ((t (:foreground "{{ color3 }}" :background "{{ background }}"))))
    '(confusingly-reordered ((t (:foreground "{{ color2 }}" :background "{{ background }}"))))
    '(edmacro-label ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(elisp-shorthand-font-lock-face ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(error ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(file-name-shadow ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(help-argument-name ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(help-key-binding ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(ibuffer-locked-buffer ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(icon-button ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(menu ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(mm-command-output ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(mm-uu-extract ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(nobreak-hyphen ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(nobreak-space ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(pgtk-im-0 ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(read-multiple-choice-face ((t (:foreground "{{ color4 }}" :background "{{ background }}"))))
    '(rectangle-preview ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(separator-line ((t (:foreground "{{ color4 }}" :background "{{ background }}"))))
    '(success ((t (:foreground "{{ color2 }}" :background "{{ background }}"))))

    '(fixed-pitch ((t (:family "Monospace"))))
    '(variable-pitch ((((type w32)) (:foundry "outline" :family "Arial")) (t (:family "Sans Serif"))))

    '(escape-glyph ((t (:weight bold :foreground "{{ color2 }}"))))
    '(homoglyph ((t (:weight bold :foreground "{{ color3 }}"))))

    '(minibuffer-prompt ((t (:foreground "{{ color11 }}"))))

    '(highlight ((t (:background "{{ current_line }}" :color foreground-color))))
    '(lazy-highlight ((t (:foreground "{{ color4 }}" :background "{{ color1 }}"))))

    '(region ((t (:extend t :background "{{ select }}"))))

    '(secondary-selection ((t (:extend t :foreground "{{ background }}" :background "{{ color2 }}"))))

    '(button ((t (:foreground "{{ color3 }}" :background "{{ background }}"))))

    '(link ((t (:underline (:color foreground-color :style line :position nil) :foreground "{{ color4 }}"))))
    '(link-visited ((t (:underline (:color foreground-color :style line :position nil) :foreground "{{ color5 }}"))))

    '(header-line ((t (:foreground "{{ color2 }}" :background "{{ background }}"))))
    '(header-line-highlight ((t (:foreground "{{ color3 }}" :background "{{ background }}"))))

    '(tooltip
        ((((class color)) ( :inherit (variable-pitch) :foreground "black" :background "{{ color11 }}")) (t (:inherit (variable-pitch)))))

    '(shadow
        ((((class color grayscale) (min-colors 88) (background light)) (:foreground "{{ listchar }}"))
        (((class color grayscale) (min-colors 88) (background dark)) (:foreground "{{ color2 }}")) ;; org keys
        (((class color) (min-colors 8) (background light)) (:foreground "{{ color5 }}"))
        (((class color) (min-colors 8) (background dark)) (:foreground "{{ color1 }}"))))

    '(trailing-whitespace ((((class color) (background light)) (:background "{{ color1 }}"))
        (((class color) (background dark)) (:background "{{ color3 }}")) (t (:inverse-video t))))

    '(font-lock-builtin-face ((t (:foreground "{{ color5 }}"))))
    '(font-lock-comment-face ((t (:foreground "{{ comment }}"))))
    '(font-lock-constant-face ((t (:foreground "{{ color5 }}"))))
    '(font-lock-function-name-face ((t (:foreground "{{ color2 }}"))))
    '(font-lock-keyword-face ((t (:weight bold :foreground "{{ color4 }}"))))
    '(font-lock-negation-char-face ((t nil)))
    '(font-lock-number-face ((t nil)))
    '(font-lock-operator-face ((t nil)))
    '(font-lock-punctuation-face ((t nil)))
    '(font-lock-string-face ((t (:foreground "{{ color2 }}"))))
    '(font-lock-type-face ((t (:weight bold :foreground "{{ color10 }}"))))
    '(font-lock-variable-name-face ((t (:foreground "{{ color10 }}"))))
    '(font-lock-warning-face ((t (:foreground "{{ color5 }}"))))

    '(font-lock-regexp-grouping-backslash ((t (:inherit (bold)))))
    '(font-lock-regexp-grouping-construct ((t (:inherit (bold)))))

    '(font-lock-bracket-face ((t (:inherit (font-lock-punctuation-face)))))
    '(font-lock-comment-delimiter-face ((default (:inherit (font-lock-comment-face)))))
    '(font-lock-delimiter-face ((t (:inherit (font-lock-punctuation-face)))))
    '(font-lock-doc-face ((t (:inherit (font-lock-string-face)))))
    '(font-lock-doc-markup-face ((t (:inherit (font-lock-constant-face)))))

    '(font-lock-escape-face ((t (:inherit (font-lock-regexp-grouping-backslash)))))
    '(font-lock-function-call-face ((t (:inherit (font-lock-function-name-face)))))
    '(font-lock-misc-punctuation-face ((t (:inherit (font-lock-punctuation-face)))))
    '(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
    '(font-lock-property-name-face ((t (:inherit (font-lock-variable-name-face)))))
    '(font-lock-property-use-face ((t (:inherit (font-lock-property-name-face)))))
    '(font-lock-variable-use-face ((t (:inherit (font-lock-variable-name-face)))))

    '(info-index-match ((t (:foreground "{{ color0 }}" :background "{{ color2 }}" ))))

    '(mode-line ((t (:foreground "{{ foreground }}" :background "{{ current_line }}"))))
    '(mode-line-buffer-id ((t (:foreground "{{ background }}" :background "{{ color2 }}" :weight bold))))
    '(mode-line-emphasis ((t (:foreground "{{ color0 }}" :background "{{ color4 }}" :weight bold))))
    '(mode-line-highlight ((((supports :box t)
        (class color) (min-colors 88)) (:box (:line-width (2 . 2) :color "{{ color0 }}" :style released-button)))
        (t (:inherit (highlight)))))
    '(mode-line-inactive ((t (:foreground "{{ current_line }}" :background "{{ color15 }}"))))

    '(isearch ((t (:foreground "{{ color3 }}" :background "{{ background }}"))))
    '(isearch-fail ((((class color) (min-colors 88) (background light)) (:background "{{ color1 }}"))
        (((class color) (min-colors 88) (background dark)) (:background "{{ color1 }}"))
        (((class color) (min-colors 16)) (:background "{{ color1 }}"))
        (((class color) (min-colors 8)) (:background "{{ color1 }}"))
        (((class color grayscale)) (:foreground "{{ color15 }}")) (t (:inverse-video t))))

    '(match ((((class color) (min-colors 88) (background light)) (:background "{{ color5 }}"))
        (((class color) (min-colors 88) (background dark)) (:background "{{ color4 }}"))
        (((class color) (min-colors 8) (background light)) (:foreground "{{ color0 }}" :background "{{ color3 }}"))
        (((class color) (min-colors 8) (background dark)) (:foreground "{{ color7 }}" :background "{{ color4 }}"))
        (((type tty) (class mono)) (:inverse-video t)) (t (:background "{{ color15 }}"))))

    '(next-error ((t (:inherit (region)))))
    '(query-replace ((t (:inherit (isearch)))))

    '(gnus-header-content ((t (:weight normal :foreground "{{ color11 }}"))))
    '(gnus-header-from ((t (:foreground "{{ color1 }}" :weight bold))))
    '(gnus-header-subject ((t (:foreground "{{ color2 }}"))))
    '(gnus-header-name ((t (:foreground "{{ color6 }}"))))
    '(gnus-header-newsgroups ((t (:foreground "{{ color5 }}"))))

    '(ansi-color-black ((t (:background "{{ color0 }}" :foreground "{{ color0 }}"))))
    '(ansi-color-red ((t (:background "{{ color1 }}" :foreground "{{ color1 }}"))))
    '(ansi-color-green ((t (:background "{{ color2 }}" :foreground "{{ color2 }}"))))
    '(ansi-color-yellow ((t (:background "{{ color3 }}" :foreground "{{ color3 }}"))))
    '(ansi-color-blue ((t (:background "{{ color4 }}" :foreground "{{ color4 }}"))))
    '(ansi-color-magenta ((t (:background "{{ color5 }}" :foreground "{{ color5 }}"))))
    '(ansi-color-cyan ((t (:background "{{ color6 }}" :foreground "{{ color6 }}"))))
    '(ansi-color-white ((t (:background "{{ color7 }}" :foreground "{{ color7 }}"))))
    '(ansi-color-bright-black ((t (:background "{{ color8 }}" :foreground "{{ color8 }}"))))
    '(ansi-color-bright-red ((t (:background "{{ color9 }}" :foreground "{{ color9 }}"))))
    '(ansi-color-bright-green ((t (:background "{{ color10 }}" :foreground "{{ color10 }}"))))
    '(ansi-color-bright-yellow ((t (:background "{{ color11 }}" :foreground "{{ color11 }}"))))
    '(ansi-color-bright-blue ((t (:background "{{ color12 }}" :foreground "{{ color12 }}"))))
    '(ansi-color-bright-magenta ((t (:background "{{ color13 }}" :foreground "{{ color13 }}"))))
    '(ansi-color-bright-cyan ((t (:background "{{ color14 }}" :foreground "{{ color14 }}"))))
    '(ansi-color-bright-white ((t (:background "{{ color15 }}" :foreground "{{ color15 }}"))))
)

(custom-set-faces
    '(org-warning ((t (:foreground "{{ background }}" :background "{{ color2 }}"))))
    '(org-headline-warning ((t (:foreground "{{ color4 }}" :background "{{ color4 }}"))))

    '(org-todo ((t (:foreground "{{ color11 }}" :background "{{ background }}"))))
    '(org-headline-todo ((t (:foreground "{{ background }}" :background "{{ color0 }}"))))

    '(org-done ((t (
                    :box (:line-width 1 :color "{{ color2 }}")
                    :foreground "{{ foreground }}"
                    :background "{{ background }}"
                    :weight bold))))
    '(org-headline-done ((t (
                             :foreground "{{ color2 }}"
                             :background "{{ background }}"
                             :weight bold))))

    '(org-faces ((t (:foreground "{{ color1 }}" :background "{{ color4 }}"))))
    '(org-headline-faces ((t (:foreground "{{ color1 }}" :background "{{ color4 }}"))))

    '(org-block-begin-line
        ((t (:underline "{{ color4 }}" :foreground "{{ color4 }}" :background "{{ background }}"))))
    '(org-block-background
        ((t (:background "{{ color3 }}"))))
    '(org-block-end-line
        ((t (:overline "{{ color4 }}" :foreground "{{ color4 }}" :background "{{ background }}"))))

    '(org-level-1 ((t (:height 1.0 :foreground "{{ color5 }}"  :bold t))))
    '(org-level-2 ((t (:height 1.0 :foreground "{{ color5 }}" :bold t))))
    '(org-level-3 ((t (:height 1.0 :foreground "{{ color5 }}" :bold t :italic t))))
    '(org-level-4 ((t (:height 1.0 :foreground "{{ color5 }}" :italic t))))
    '(org-level-5 ((t (:height 1.0 :foreground "{{ color5 }}"))))
    '(org-document-title ((t (,@headline ,@variable-tuple :foreground "{{ color6 }}" :height 1.2 :underline nil))))
    '(org-table ((t (:foreground "{{ color12 }}" :bold t))))
    '(org-table-header ((t (:foreground "{{ color2 }}" :bold t))))

    '(org-modern-todo ((t (:foreground "{{ color1 }}" :background "{{ background }}"))))
    '(org-modern-done ((t (:foreground "{{ color2 }}" :background "{{ background }}"))))

    '(custom-button ((t :foreground "{{ color1 }}")))
    '(custom-button-mouse ((t :foreground "{{ color1 }}")))
    '(custom-button-pressed ((t :foreground "{{ color1 }}")))
    '(custom-changed ((t :foreground "{{ color2 }}")))
    '(custom-comment ((t :foreground "{{ color4 }}")))
    '(custom-comment-tag ((t :foreground "{{ color2 }}")))
    '(custom-invalid ((t :foreground "{{ color1 }}")))
    '(custom-modified ((t :foreground "{{ color3 }}")))
    '(custom-rogue ((t :foreground "{{ color1 }}")))
    '(custom-set ((t :foreground "{{ color2 }}")))
    '(custom-state ((t :foreground "{{ color4 }}")))
    '(custom-themed ((t :foreground "{{ color5 }}")))
    '(custom-variable-obsolete ((t :foreground "{{ color1 }}")))
    '(custom-face-tag ((t :foreground "{{ color0 }}")))
    '(custom-group-tag ((t :foreground "{{ color2 }}")))
    '(custom-group-tag-1 ((t :foreground "{{ color3 }}")))
    '(custom-variable-tag ((t :foreground "{{ color4 }}")))

    '(highlight-numbers-number ((t (:foreground "{{ background }}" :background "{{ color2 }}"))))

    '(show-paren-match ((t :foreground "{{ color1 }}" :background "{{ comment }}" :underline nil :bold t)))

    '(line-number ((t :foreground "{{ comment }}" :background "{{ background }}" :underline nil :bold nil)))
    '(line-number-current-line ((t :foreground "{{ color11 }}" :background "{{ background }}" :underline nil :bold nil)))

    '(custom-declare-face '+org-todo-idea ((t :weight bold :foreground "#94e2d5")))
    '(custom-declare-face '+org-todo-canceled ((t :weight bold :foreground "{{ color1 }}")))
)

(setq org-todo-keywords
    '((sequence "TODO" "STARTED" "WAITING" "CANCELED" "|" "DONE"))
)

(setq org-todo-keyword-faces '(
    ("TODO" . (:foreground "{{ color3 }}" :background "{{ background }}" :height 140 :weight bold))
    ("STARTED" . (:foreground "{{ color3 }}" :background "{{ background }}" :height 140 :weight bold))
    ("WAITING" . (:foreground "{{ color3 }}" :background "{{ background }}" :height 140 :weight bold))
    ("CANCELED" . (:foreground "{{ color1 }}" :background "{{ background }}" :height 140 :weight bold :strike-through t))
    ("DONE" . (:foreground "{{ color2 }}" :background "{{ background }}" :height 140 :weight bold))
))

(setq org-format-latex-options '(
    :foreground "{{ color3 }}"
    :background default
    :scale 1.35
    :html-foreground "Black"
    :html-background "Transparent"
    :html-scale 1.0
    :matchers ("begin" "$1" "$" "$$" "\\(" "\\["))
)

(provide-theme '{{ key_name }})
