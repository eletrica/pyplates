local colors = {
    background    =  '{{ background }}',
    foreground    =  '{{ foreground }}',
    listchar      =  '{{ listchar }}',
    current_line  =  '{{ current_line }}',
    comment       =  '{{ comment }}',
    color0        =  '{{ color0 }}',
    color1        =  '{{ color1 }}',
    color2        =  '{{ color2 }}',
    color3        =  '{{ color3 }}',
    color4        =  '{{ color4 }}',
    color5        =  '{{ color5 }}',
    color6        =  '{{ color6 }}',
    color7        =  '{{ color7 }}',
}

local M =  {
    normal = {
        a = {bg = colors.comment, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    insert = {
        a = {bg = colors.color4, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color4},
    },
    visual = {
        a = {bg = colors.color3, fg = colors.background, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.color3},
    },
    replace = {
        a = {bg = colors.color1, fg = colors.color0, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color1},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    command = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.current_line, fg = colors.foreground},
    },
    inactive = {
        a = {bg = colors.background, fg = colors.foreground, gui = 'bold'},
        b = {bg = colors.listchar, fg = colors.color2},
        c = {bg = colors.background, fg = colors.foreground},
    },
    diff_color = {
        added = {bg = colors.listchar, fg = colors.color2},
        modified = {bg = colors.listchar, fg = colors.color3},
        removed = {bg = colors.listchar, fg = colors.color1},
    },
}

M.terminal = M.normal

return M
