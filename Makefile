# ==============================================================================
# Program:       Makefile
# Description:   targets to use pyplates script
# ==============================================================================

main = main.py
pyplates = @python $(main)

nvim-tpl = color.vim
nvim-dir = $(HOME)/.config/nvim/colors

micro-tpl = base.micro
micro-dir = $(HOME)/.config/micro/colorschemes

waybar-tpl = waybar.css
waybar-dir = $(HOME)/.config/waybar/themes

lualine-tpl = lualine.lua
lualine-dir = $(HOME)/.config/nvim/lua/lualine/themes

hypr-tpl = hypr-colors.conf
hypr-dir = $(HOME)/.config/hypr/colors

emacs-tpl = emacs-theme.el
emacs-dir = $(HOME)/.emacs.d/colorschemes

default:
	$(pyplates) -h

nvim:
	$(pyplates) -t $(nvim-tpl) -o $(nvim-dir)

lualine:
	$(pyplates) -t $(lualine-tpl) -o $(lualine-dir)

micro:
	$(pyplates) -t $($@-tpl) -o $($@-dir)

all: nvim lualine micro

.PHONY: default all nvim lualine micro
