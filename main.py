#!/usr/bin/env python

# ==============================================================================
# Program:       pyplates
# Description:   Generate files using jinja templates
# Software/Tool: python(3.10.5)/jinja2/toml
# ==============================================================================
"""
Usage:
Choose correct template and scheme to generate output file
"""

import os
import argparse

from hashlib import md5
from jinja2 import FileSystemLoader
from jinja2 import Environment

import toml

# set environment variables
home_dir = os.getenv('HOME')

# jinja config
loader = FileSystemLoader('templates')
env = Environment(loader=loader)

# create arg parsers
parser = argparse.ArgumentParser(description='Make files from jinja templates.')
parser.add_argument(
    '-t',
    '--template',
    type=str,
    metavar='template',
    dest='f_template',
    help='Choose one from templates folder',
)
parser.add_argument(
    '-d',
    '--data',
    type=str,
    default='bases/colors.toml',
    metavar='toml-data',
    dest='f_data',
    help='Choose one from data folder',
)
parser.add_argument(
    '-o',
    '--output',
    type=str,
    default=f'{home_dir}/Downloads',
    metavar='output-directory',
    dest='output_dir',
    help='Default: ~/Downloads',
)
parser.add_argument(
    '-k',
    '--key',
    type=str,
    metavar='key value',
    dest='key_name',
    help='Type a valid key from toml file',
)
parser.add_argument(
    '-f',
    '--file',
    type=str,
    metavar='filename',
    dest='file_name',
    help='Output file name',
)
args = parser.parse_args()

# define default values and replace case empty
if args.f_template is not None:
    f_template = args.f_template

if args.f_data is not None:
    f_data = args.f_data

if args.output_dir is not None:
    output_dir = args.output_dir

# save file extension
filename, f_ext = os.path.splitext(f_template)

# define jinja template and data values
toml_data = toml.load(f_data)
file_keys = toml_data.keys()
base_template = env.get_template(f_template)


def print_keys(keys) -> None:
    """print keys from toml file"""
    for key_name in keys:
        print(f'- {key_name}')


def main() -> None:
    """main"""
    if args.key_name is not None:
        key_name = args.key_name

        if key_name in file_keys:
            data_scheme = toml_data[key_name]
            uuid = md5(key_name.encode('utf-8'))
            data_scheme['uuid'] = uuid.hexdigest()
            data_scheme['key_name'] = key_name
            out_file = base_template.render(data_scheme)
            print(f'Generate {key_name} scheme...')
            print(f'Saving in {output_dir}.')
            file_name = key_name

            if filename == "emacs-theme":
                file_name = f'{file_name}-theme'

            if args.file_name is not None:
                file_name = args.file_name

            with open(f'{output_dir}/{file_name}{f_ext}', 'wt', encoding='utf-8') as f:
                f.write(out_file)
            print('Done.')
        else:
            print('Colorscheme not found. Choose one above:')
            print_keys(file_keys)
            print('Pass no -k argument to render all.')
    else:
        # generate files for all keys if key_name is empty
        print('Generating colorschemes...')
        print(f'Saving in {output_dir}.')
        for key_name in file_keys:
            uuid = md5(key_name.encode('utf-8'))
            toml_data[key_name]['key_name'] = key_name
            toml_data[key_name]['uuid'] = uuid.hexdigest()
            out_file = base_template.render(toml_data[key_name])

            file_name = key_name

            if filename == "emacs-theme":
                file_name = f'{file_name}-theme'

            with open(f'{output_dir}/{file_name}{f_ext}', 'wt', encoding='utf-8') as f:
                f.write(out_file)
        print('Done.')


# run only in self, not in module
if __name__ == '__main__':
    main()
